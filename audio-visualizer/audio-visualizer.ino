// https://www.norwegiancreations.com/2017/08/what-is-fft-and-how-can-you-implement-it-on-an-arduino/

#include "FastLED.h"
#include "arduinoFFT.h"

//#define COUNT 300

#define SAMPLES 64       //Must be a power of 2
#define SAMPLING_FREQUENCY 1000 //Hz, must be less than 10000 due to ADC


arduinoFFT FFT = arduinoFFT();
CRGB leds[300];


unsigned int sampling_period_us;
unsigned long microseconds;
unsigned int color;

double vReal[SAMPLES];
double vImag[SAMPLES];


void setup() {
    FastLED.addLeds<NEOPIXEL, 6>(leds, 300);

    Serial.begin(115200);

    sampling_period_us = round(1000000*(1.0/SAMPLING_FREQUENCY));
    color = 255*255*5+255*100;
  }

void loop() {
  if(color > 255*255*255+255*255+255){
    color = 0;
  }
  color = color+1;

  /*SAMPLING*/
  for(int i=0; i<SAMPLES; i++)
  {
    microseconds = micros();  //Overflows after around 70 minutes!
  
    vReal[i] = analogRead(0);
    vImag[i] = 0;
  
    while(micros() < (microseconds + sampling_period_us)){
    }
  }

  /*FFT
  FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
  FFT.Compute(vReal, vImag, SAMPLES, FFT_FORWARD);
  FFT.ComplexToMagnitude(vReal, vImag, SAMPLES);
  double peak = FFT.MajorPeak(vReal, SAMPLES, SAMPLING_FREQUENCY);

for(int i=0; i<(SAMPLES/2); i++)
  {
    /*View all these three lines in serial terminal to see which frequencies has which amplitudes
    
    Serial.print((i * 1.0 * SAMPLING_FREQUENCY) / SAMPLES, 1);
    Serial.print(" ");
    Serial.println(vReal[i], 1);  //View only this line in serial plotter to visualize the bins
  }
  /*PRINT RESULTS
 // Serial.println(peak);   //Print out what frequency is the most dominant.
  */
  FastLED.showColor(color);


   //delay(100);  //Repeat the process every second OR:
  //while(1);   
  

}
