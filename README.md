The goal of this project is to make LEDs blink to the music/audio input.

The setup:
- WS2812B RGB-LED-Strip
- Arduino Nano
- Old Headphone cable
- Old USB cable or powering the LED strip

The arduino gets audio input from the headphone cable through the A0 analog-pin, computes it and sends and output signal through the D6 digital pin to the LED-strip.
